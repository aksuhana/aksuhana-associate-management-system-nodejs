const express = require("express");
const Router = require("express");
const INDEX_NAME = "development";

const router = Router();

router.get('/', (req, res) => {
    res.send(`app-root, ${INDEX_NAME} mode`);
});
module.exports = router.use(require('../modules/assoicate_module/router'))
