exports.response = async (success, message, data, code, is_message_show, res) => {
  console.log(success, message, data, code, is_message_show);
  const result = {
    success: success,
    message: message,
    data: data,
    code: code,
    is_message_show: is_message_show
  }
  return res.send(result);
}

