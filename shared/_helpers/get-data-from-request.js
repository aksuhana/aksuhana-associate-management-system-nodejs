const dataFromRequest = function (req) {
    let propMap = Object.assign(req.query, req.body);
    return propMap;
  };
  
  module.exports = {
    dataFromRequest,
  };
  