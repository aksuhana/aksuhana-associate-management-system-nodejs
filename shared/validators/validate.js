'use strict';
const _ = require('underscore');
const { validator } = require('express-validator');
const { body, validationResult, check } = require('express-validator/check');
const ResponceService = require('../_helpers/response');
const createError = require('http-errors');
const Ajv = require('ajv');

const execValidation = (req, res, next) => {
    const result = validationResult(req);
    if (!result.isEmpty()) {
        ResponceService.response(false,result.errors[0].msg, result, 422, true, res)
    }
    return next();
};

const validate = (validationChains) => {
    return [ ...validationChains, execValidation ];
};
module.exports = validate;