const express = require('express');
const router = express.Router();
const validate = require('../../shared/validators/validate');
const AssociateController = require('./controllers/associate.controller');
const Validator = require("../assoicate_module/validators/user.chain");

router.post('/save/master/specialization', AssociateController.saveMasterSpecialization);
router.post('/save/associate',
    //validate(Validator.saveAssociate),
    AssociateController.saveAssociate);
router.get('/get/associate/:id',
    AssociateController.getAssociate);
router.delete('/delete/associate',
    validate(Validator.deleteAssociate),
    AssociateController.deleteAssociate);
router.get('/search/associate',
    validate(Validator.searchKey),
    AssociateController.searchAssociate);
module.exports = router;


