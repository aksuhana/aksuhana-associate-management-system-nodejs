'use strict';
const { check, } = require('express-validator/check');

const deleteAssociate = [

    check('_id').exists().isMongoId().not()
    .isEmpty().isString().withMessage("a valid mongo _id is required to delete "),
];
const saveAssociate = [
    check('AssociateName').exists().not()
        .isEmpty().withMessage("Associate Name is required"),
    check('Phone').exists().not()
        .isEmpty().isLength(10).withMessage("Phone is required with length 10"),
    check('Address').exists().not().withMessage("Address is required"),
    check('Specialization').exists().isMongoId().not()
        .isEmpty().isArray().withMessage("Specialization has to be an array with mongo ID"),
];
const searchKey =[
check('search').optional({ checkFalsy: true }).isString(),
    ]
module.exports = {
  deleteAssociate,
    saveAssociate,
    searchKey
}

