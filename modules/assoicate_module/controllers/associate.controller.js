const AssociateService = require('../services/associate.service');
const { dataFromRequest } = require("../../../shared/_helpers/get-data-from-request");



exports.saveAssociate = async (req, res) => {
    await AssociateService.saveAssociate(req, res);
}

exports.getAssociate = async (req, res) => {

    await AssociateService.getAssociate(req, res);
}

exports.deleteAssociate = async (req, res) => {

    await AssociateService.deleteAssociate(req, res);
}
exports.searchAssociate = async (req, res) => {

    await AssociateService.searchAssociate(req, res);
}
exports.saveMasterSpecialization = async (req, res) => {

    await AssociateService.saveMasterSpecialization(req, res);
}



