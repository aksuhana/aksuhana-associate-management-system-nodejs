const mongoose = require('mongoose');

const specializationSchema = new mongoose.Schema({

  SpecializationName: {
    type: String,
    required: true
  },


}, { timestamps: true });

module.exports = mongoose.model('specialization_master', specializationSchema);

