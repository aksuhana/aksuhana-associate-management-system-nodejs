const mongoose = require('mongoose');

const associateSchema = new mongoose.Schema({


    AssociateName: {
        type: String,
        required: true

    },
    Phone: {
        type: String,
        required: true,
    },
    Address: {
        type: String,
        required: true,

    },
    Specialization: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'specialization_master',
    }]
}, {timestamps: true});

module.exports = mongoose.model('associates', associateSchema);
