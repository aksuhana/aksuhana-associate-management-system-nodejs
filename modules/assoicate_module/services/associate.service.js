const ResponceService = require('../../../shared/_helpers/response');
const Associate = require("../models/associates");
const Specialization = require("../models/specialization");
const { dataFromRequest } = require("../../../shared/_helpers/get-data-from-request");





exports.saveMasterSpecialization = async (req, res) => {
	const specialMaster = new Specialization(req.body);
	specialMaster.save((error, data) => {
		if (error) ResponceService.response(false, error, null, 400, true, res);
		if (data) {
			return ResponceService.response(true, "User Created successfully", data, 200, true, res);
		}
	});

}



exports.saveAssociate = async (req, res) => {
	const associate = new Associate(dataFromRequest(req));
	associate.save((error, data) => {
		if (error) ResponceService.response(false, error, null, 400, true, res);
		if (data) {
			console.log("adfas");
			return ResponceService.response(true, "User Created successfully", data, 200, true, res);
		}
	});

}



 exports.getAssociate = async(req,res)=> {
		const data = await Associate.find({_id: req.params.id})
		.populate('Specialization').exec()
		if(!data)
			 ResponceService.response(false, "No such associate exists", data, 200, true, res);
		     ResponceService.response(true, "Associate listed successfully", data, 200, true, res);

}

exports.deleteAssociate = async(req,res)=> {
	Associate.deleteOne({_id: req.body._id}, function (err, data) {
		if(!data.deletedCount)
		ResponceService.response(false, "No such associate exists to delete", [], 200, true, res);
		ResponceService.response(true, "Associate deleted successfully", [], 200, true, res);
	})
}

exports.searchAssociate = async(req,res)=> {
	 const searchParam =  dataFromRequest(req, 'search');
	 const data = await Associate.find({AssociateName:{ $regex: new RegExp(searchParam.search, 'i') }})
		.populate('Specialization').exec();
		if(!data.length)
			ResponceService.response(false, "No such associate exists", data, 200, true, res);
			ResponceService.response(true, "Search result(s) found", data, 200, true, res);

}
