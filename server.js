require('./config/db');
const express = require("express");
const app = express();
const chalk = require('chalk');
const mongoose = require("mongoose");
const path = require('path');
const cors = require('cors');
const FileSize = require('./shared/constants/file.size')
app.use(cors());
app.use(express.urlencoded({ limit: FileSize.FILESIZE.SIZE, extended: true }));
app.use(express.json({ limit: FileSize.FILESIZE.SIZE }));
app.use(express.static(path.join(__dirname, '/uploads')));

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}
app.get("/", (req, res) => {
  res.send(`Welcome to API, Server started on port: ${PORT}`);
});
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

const prefix = process.env.END_POINT_PREFIX;
app.use(prefix, require('./core/modules'));
const PORT = process.env.PORT || 1100;
mongoose.connection
  .on('open', () => console.log('🚀  MongoDB: Connection Succeeded'))
  .on('error', err => console.error("err"));
app.listen(PORT, () => {
  console.log(chalk.blue(`Listening on port:${PORT}`));
});
