

## Topics
1. [Introduction](#introduction)
2. [Installation & Configuration](#installation-and-configuration)

### Introduction

SAGUNA wants to create an Associate Management System to manage its associates in the organization
using Node.js.
1. Create a ‘Specialization Master’ having fields SpecializationId (Primary Key) and
   SpecializationName to manage the specializations in the system.
2. Some specialization names are like Angular, ASP.Net, Web API, Knockout, SharePoint,
   Nodejs, React.js etc.
3. Create a ‘Associates Master’ having fields AssociateId (Primary Key), AssociayeName,
   Phone, Address, SpecializationId (Foreign Key) to record the associates within the system.
4. Each Associate can have one or more Specializations like, ASP.Net Web API, Knockout,
   SharePoint, Node.js, React.js etc.
### Running application on local

** Go to project directory.**

##### 1. copy .env-copy to .env
##### 2. run npm install
##### 3. run node server.js

###  Screenshots of API's response.

<img src="https://i2.paste.pics/e85d8e418d1fb4c36480943f02679194.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0"/>
<img src="https://i2.paste.pics/9f6d28dac535806214cc37150655a5de.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0"/>
<img src="https://i2.paste.pics/e5d2069a0d105ea57f60145203dc5c10.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0" alt="">
<img src="https://i2.paste.pics/9c5da31b43d39784b3afb026222d985b.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0" alt="">
<img src="https://i2.paste.pics/e0a2e0688ae19c692eb5c8b03078cd35.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0" alt="">
<img src="https://i2.paste.pics/f608b82eb84a356d42310a19250bd481.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0" alt="">
<img src="https://i2.paste.pics/0fdcbe5726c03889d664a24c9307d7b2.png?trs=cdf1386e193ed51bd36eaa07e8bc361ca0fabdd69cec57514cbf7ba97c7263e0" alt="">